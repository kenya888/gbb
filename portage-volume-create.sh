#!/bin/env bash

PORTAGE_PODMAN_VOLUME_NAME=portage
PORTAGE_ARCHIVE_FILE=portage-latest.tar.bz2
PORTAGE_ARCHIVE_URL=http://ftp.iij.ad.jp/pub/linux/gentoo/snapshots/${PORTAGE_ARCHIVE_FILE}

if [ ! -e ${PORTAGE_ARCHIVE_FILE} ]; then
  curl -LO ${PORTAGE_ARCHIVE_URL}
fi

if  ! $(podman volume exists ${PORTAGE_PODMAN_VOLUME_NAME}); then
  podman volume create portage
fi

tar xf ${PORTAGE_ARCHIVE_FILE}

cd ./portage
tar cf - ./ | podman volume import portage -

podman volume list
