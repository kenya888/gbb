#!/bin/env bash

IMAGE=9cedf410f76f
VOLUME_NAME=portage

podman run -it --rm -v ${VOLUME_NAME}:/var/db/repos/gentoo:rw ${IMAGE} emerge --sync
