FROM docker.io/gentoo/portage:latest as portage

FROM docker.io/gentoo/stage3:amd64-systemd-20230123 as base

COPY --from=portage /var/db/repos/gentoo /var/db/repos/gentoo
COPY files/make.conf /etc/portage/make.conf
COPY files/gentoo.conf /etc/portage/repos.conf/gentoo.conf

RUN emerge --sync && emerge -v --buildpkg=y --usepkg=y gcc:12 && emerge -C gcc:11 && env-update && emerge -v sys-apps/merge-usr && merge-usr && eselect profile set default/linux/amd64/17.1/systemd/merged-usr

FROM base as clang-bootstrap

COPY files/use-clang /etc/portage/package.use/clang

RUN emerge -v --buildpkg=n --usepkg=n clang llvm compiler-rt llvm-libunwind lld mold && env-update && source /etc/profile

FROM clang-bootstrap as clang-complete

COPY files/compiler-clang /etc/portage/env/compiler-clang
COPY files/no-mold /etc/portage/env/no-mold
COPY files/package.env /etc/portage/package.env

RUN emerge -v --buildpkg=y --usepkg=y clang llvm compiler-rt llvm-libunwind lld

FROM clang-complete

RUN emerge -uvDN --with-bdeps=y --complete-graph=y --buildpkg=y --usepkg=y @world && binutils-config x86_64-pc-linux-gnu-2.39 && emerge -v @preserved-rebuild && emerge -v --buildpkg=y --usepkg=y ccache sys-devel/icecream dev-vcs/git && rm -rf /var/db/repos/gentoo
