#!/bin/env bash

set -x

SRC_URI=http://ftp.iij.ad.jp/pub/linux/gentoo
DATAROOT=/data/root
STAGE3_DATE="20221211T170150Z"
STAGE3_FILE="stage3-amd64-systemd-mergedusr-${STAGE3_DATE}.tar.xz"
STAGE3_URL="${SRC_URI}/releases/amd64/autobuilds/current-stage3-amd64-systemd-mergedusr/${STAGE3_FILE}"
PORTAGE_URL="${SRC_URI}/snapshots/portage-latest.tar.xz"

GCC_PROFILE=x86_64-pc-linux-gnu-12

cd /data/root
curl -LO ${STAGE3_URL}
tar xpf ./stage3-*.tar.xz --xattrs-include='*.*' --numeric-owner
rm ${STAGE3_FILE}

cd /

curl -LO ${PORTAGE_URL}
tar xvf ./portage-latest.tar.xz -C /var/db/repos
mv /var/db/repos/portage /var/db/repos/gentoo

emerge --sync

emerge --root=/data/root --sysroot=/data/root --config-root=/data/configroot -v gcc:12
emerge --root=/data/root --sysroot=/data/root --config-root=/data/configroot -C gcc:11

eselect gcc set ${GCC_PROFILE}

. /etc/profile

emerge --root=/data/root --sysroot=/data/root --config-root=/data/configroot -v @preserved-rebuild
emerge --root=/data/root --sysroot=/data/root --config-root=/data/configroot -v @world
