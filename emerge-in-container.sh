#!/bin/env bash

# Set your machines name
BUILDNAME=data

CURRENTDIR=$(pwd)
BUILDERROOT=${CURRENTDIR}/${BUILDNAME}
IMAGE=localhost/gentoo-build-container:latest

# Create portage volume
bash portage-volume-create.sh

# Build binpkgs
podman run -it --rm -v portage:/var/db/repos/gentoo:rw -v kenya888-gentoo-repo:/var/db/repos/kenya888-gentoo-repo:rw -v ${BUILDERROOT}/kernel:/usr/src/linux:ro -v ${CURRENTDIR}/cache/target/binpkgs:/var/cache/binpkgs:rw -v ${CURRENTDIR}/cache/target/distfiles:/var/cache/distfiles:rw -v ${BUILDERROOT}/configroot:/configroot:rw -v ${CURRENTDIR}/cache/ccache:/var/cache/ccache:rw ${IMAGE} emerge -v --config-root=/configroot --buildpkg=y --usepkg=y --complete-graph=y --with-bdeps=y $@ 

#podman run -it --rm -v portage:/var/db/repos/gentoo:rw -v kenya888-gentoo-repo:/var/db/repos/kenya888-gentoo-repo:rw -v ${CURRENTDIR}/data/kernel:/usr/src/linux:ro -v ${CURRENTDIR}/cache/target/binpkgs:/var/cache/binpkgs:rw -v ${CURRENTDIR}/cache/target/distfiles:/var/cache/distfiles:rw -v ${CURRENTDIR}/data/configroot:/data/configroot:rw -v ${CURRENTDIR}/cache/ccache:/var/cache/ccache:rw ${IMAGE} /bin/bash
