#!/bin/bash

set -x

TAG=latest

podman build -t gentoo-build-container:${TAG} -v $(pwd)/cache/binpkgs:/var/cache/binpkgs:rw -v $(pwd)/cache/distfiles:/var/cache/distfiles:rw .
